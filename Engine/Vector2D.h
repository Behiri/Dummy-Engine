#pragma once

namespace Math
{
	struct Vector2D
	{
		float x;
		float y;
	};

	inline Vector2D operator+(const Vector2D& left, const Vector2D& right)
	{
		return Vector2D{ left.x + right.x, left.y + right.y };
	}

	inline Vector2D operator*(float scaler, const Vector2D& right)
	{
		return Vector2D{ scaler * right.x, scaler * right.y };

	}

	inline Vector2D operator*(const Vector2D& right, float scaler)
	{
		return scaler * right;	

	}
}