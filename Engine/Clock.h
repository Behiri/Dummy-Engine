#pragma once

namespace Timing {
	class __declspec(dllexport) Clock
	{
	public:
		bool initialize();
		bool shutdown();
		float timeElapsedLastFrame();
		void newFrame();
	};

}
