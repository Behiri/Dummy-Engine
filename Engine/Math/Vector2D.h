#pragma once

namespace Math
{
	struct Vector2D
	{
		float x;
		float y;
	};

	inline Vector2D operator+(const Vector2D& left, const Vector2D& right);
	inline Vector2D operator*(float scaler, const Vector2D& right);
	inline Vector2D operator*(const Vector2D& right, float scaler);

#include "Vector2D.inl"

}