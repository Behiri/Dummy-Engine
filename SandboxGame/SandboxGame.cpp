#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>
#include <QtWidgets/QMainWindow>
#include <QtCore/QDebug>
#include "MyGlWindow.h"
#include <windows.h>

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);
	MyGlWindow myGlWindow;
	myGlWindow.move(50, 50);
	myGlWindow.setFixedSize(QSize(1200, 600));
	myGlWindow.show();

	return a.exec();
}
