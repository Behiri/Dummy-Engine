#pragma once
#include <QtOpenGL/QGLWidget>
#include <QtCore/QTimer>

class MyGlWindow : public QGLWidget
{
	Q_OBJECT

public:
	GLuint vertexBufferId;
	QTimer myTimer;

protected:
	void initializeGL() override;
	void paintGL();

private slots:
	void myUpdate();

};

